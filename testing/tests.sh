#!/bin/bash

set -e
pwd
GORACE="haltonerror=1"
go test -race ${1} -coverprofile=model.cover.out -v ./model
go test -race ${1} -coverprofile=event.cover.out -v ./event
go test -race ${1} -coverprofile=storage.cover.out -v ./storage
go test -race ${1} -coverprofile=peer.connections.cover.out -v ./protocol/connections
go test -race ${1} -coverprofile=protocol.spam.cover.out -v ./protocol/connections/spam
go test -race ${1} -coverprofile=peer.fetch.cover.out -v ./protocol/connections/fetch
go test -race ${1} -coverprofile=peer.listen.cover.out -v ./protocol/connections/listen
go test -race ${1} -coverprofile=peer.send.cover.out -v ./protocol/connections/send
go test -race ${1} -coverprofile=peer.cover.out -v ./peer
go test -race ${1} -coverprofile=server.fetch.cover.out -v ./server/fetch
go test -race ${1} -coverprofile=server.listen.cover.out -v ./server/listen
go test -race ${1} -coverprofile=server.send.cover.out -v ./server/send
go test -race ${1} -coverprofile=server.metrics.cover.out -v ./server/metrics
go test -race ${1} -coverprofile=server.cover.out -v ./server
echo "mode: set" > coverage.out && cat *.cover.out | grep -v mode: | sort -r | \
awk '{if($1 != last) {print $0;last=$1}}' >> coverage.out
rm -rf *.cover.out
