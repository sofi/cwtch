module cwtch.im/cwtch

require (
	cwtch.im/tapir v0.1.11
	git.openprivacy.ca/openprivacy/libricochet-go v1.0.8
	github.com/c-bata/go-prompt v0.2.3
	github.com/client9/misspell v0.3.4 // indirect
	github.com/dvyukov/go-fuzz v0.0.0-20191022152526-8cb203812681 // indirect
	github.com/elazarl/go-bindata-assetfs v1.0.0 // indirect
	github.com/golang/protobuf v1.3.2
	github.com/gordonklaus/ineffassign v0.0.0-20190601041439-ed7b1b5ee0f8 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mattn/go-tty v0.0.0-20190424173100-523744f04859 // indirect
	github.com/pkg/term v0.0.0-20190109203006-aa71e9d9e942 // indirect
	github.com/stephens2424/writerset v1.0.2 // indirect
	github.com/struCoder/pidusage v0.1.2
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/lint v0.0.0-20190930215403-16217165b5de // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7
	golang.org/x/tools v0.0.0-20191108175616-46f5a7f28bf0 // indirect
)

go 1.13
