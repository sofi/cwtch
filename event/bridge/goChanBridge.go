package bridge

import (
	"cwtch.im/cwtch/event"
	"sync"
)

type goChanBridge struct {
	in         chan event.IPCMessage
	out        chan event.IPCMessage
	closedChan chan bool
	closed     bool
	lock       sync.Mutex
}

// MakeGoChanBridge returns a simple testing IPCBridge made from inprocess go channels
func MakeGoChanBridge() (b1, b2 event.IPCBridge) {
	chan1 := make(chan event.IPCMessage)
	chan2 := make(chan event.IPCMessage)
	closed := make(chan bool)

	a := &goChanBridge{in: chan1, out: chan2, closedChan: closed, closed: false}
	b := &goChanBridge{in: chan2, out: chan1, closedChan: closed, closed: false}

	go monitor(a, b)

	return a, b
}

func monitor(a, b *goChanBridge) {
	<-a.closedChan
	a.closed = true
	b.closed = true
	a.closedChan <- true
}

func (pb *goChanBridge) Read() (*event.IPCMessage, bool) {
	message, ok := <-pb.in
	return &message, ok
}

func (pb *goChanBridge) Write(message *event.IPCMessage) {
	pb.lock.Lock()
	defer pb.lock.Unlock()
	if !pb.closed {
		pb.out <- *message
	}
}

func (pb *goChanBridge) Shutdown() {
	if !pb.closed {
		close(pb.in)
		close(pb.out)
		pb.closedChan <- true
		<-pb.closedChan
	}
}
