package invites

import (
	"cwtch.im/cwtch/event"
	"cwtch.im/cwtch/peer"
)

// Fuzz import group function
func Fuzz(data []byte) int {
	peer := peer.NewCwtchPeer("fuzz")
	peer.Init(event.NewEventManager())
	err := peer.ImportGroup(string(data))
	if err != nil {
		if len(peer.GetGroups()) > 0 {
			panic("group added despite error")
		}
		return 0
	}
	return 1
}
