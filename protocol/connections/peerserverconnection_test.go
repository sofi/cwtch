package connections

import (
	"cwtch.im/cwtch/event"
	"cwtch.im/cwtch/protocol"
	"cwtch.im/cwtch/server/fetch"
	"cwtch.im/cwtch/server/send"
	"cwtch.im/tapir/primitives"
	"git.openprivacy.ca/openprivacy/libricochet-go"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/connection"
	"git.openprivacy.ca/openprivacy/libricochet-go/connectivity"
	identityOld "git.openprivacy.ca/openprivacy/libricochet-go/identity"
	"golang.org/x/crypto/ed25519"
	"net"
	"testing"
	"time"
)

func ServerAuthValid(hostname string, key ed25519.PublicKey) (allowed, known bool) {
	return true, true
}

type TestServer struct {
	connection.AutoConnectionHandler
	Received chan bool
}

func (ts *TestServer) HandleGroupMessage(gm *protocol.GroupMessage) {
	ts.Received <- true
}

func (ts *TestServer) HandleFetchRequest() []*protocol.GroupMessage {
	return []*protocol.GroupMessage{{Ciphertext: []byte("hello"), Signature: []byte{}, Spamguard: []byte{}}, {Ciphertext: []byte("hello"), Signature: []byte{}, Spamguard: []byte{}}}
}

func runtestserver(t *testing.T, ts *TestServer, priv ed25519.PrivateKey, identity primitives.Identity, listenChan chan bool) {
	ln, _ := net.Listen("tcp", "127.0.0.1:5451")
	listenChan <- true
	conn, _ := ln.Accept()
	defer conn.Close()

	rc, err := goricochet.NegotiateVersionInbound(conn)
	if err != nil {
		t.Errorf("Negotiate Version Error: %v", err)
	}
	// TODO switch from old identity to new tapir identity.
	pub := identity.PublicKey()
	err = connection.HandleInboundConnection(rc).ProcessAuthAsV3Server(identityOld.InitializeV3("", &priv, &pub), ServerAuthValid)
	if err != nil {
		t.Errorf("ServerAuth Error: %v", err)
	}

	ts.RegisterChannelHandler("im.cwtch.server.send", func() channels.Handler {
		server := new(send.CwtchServerSendChannel)
		server.Handler = ts
		return server
	})

	ts.RegisterChannelHandler("im.cwtch.server.fetch", func() channels.Handler {
		server := new(fetch.CwtchServerFetchChannel)
		server.Handler = ts
		return server
	})

	rc.Process(ts)
}

func TestPeerServerConnection(t *testing.T) {
	identity, priv := primitives.InitializeEphemeralIdentity()
	t.Logf("Launching Server....\n")
	ts := new(TestServer)
	ts.Init()
	ts.Received = make(chan bool)
	listenChan := make(chan bool)
	go runtestserver(t, ts, priv, identity, listenChan)
	<-listenChan
	onionAddr := identity.Hostname()

	manager := event.NewEventManager()
	engine := NewProtocolEngine(identity, priv, connectivity.LocalProvider(), manager, nil, nil)

	psc := NewPeerServerConnection(engine, "127.0.0.1:5451|"+onionAddr)
	numcalls := 0
	psc.GroupMessageHandler = func(s string, gm *protocol.GroupMessage) {
		numcalls++
	}
	state := psc.GetState()
	if state != DISCONNECTED {
		t.Errorf("new connections should start in disconnected state")
	}
	time.Sleep(time.Second * 1)
	go psc.Run()
	psc.WaitTilSynced()

	gm := &protocol.GroupMessage{Ciphertext: []byte("hello"), Signature: []byte{}}
	psc.SendGroupMessage(gm)

	// Wait until message is received
	<-ts.Received

	if numcalls != 2 {
		t.Errorf("Should have received 2  calls from fetch request, instead received %v", numcalls)
	}
}
