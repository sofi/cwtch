// Known race issue with event bus channel closure

package storage

import (
	"cwtch.im/cwtch/event"
	"os"
	"testing"
	"time"
)

const testProfileName = "Alice"
const testKey = "key"
const testVal = "value"
const testInitialMessage = "howdy"
const testMessage = "Hello from storage"

func TestProfileStoreWriteRead(t *testing.T) {
	os.RemoveAll(testingDir)
	eventBus := event.NewEventManager()
	profile := NewProfile(testProfileName)
	ps1 := NewProfileWriterStore(eventBus, testingDir, password, profile)

	eventBus.Publish(event.NewEvent(event.SetAttribute, map[event.Field]string{event.Key: testKey, event.Data: testVal}))
	time.Sleep(1 * time.Second)

	groupid, invite, err := profile.StartGroup("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd")
	if err != nil {
		t.Errorf("Creating group: %v\n", err)
	}
	if err != nil {
		t.Errorf("Creating group invite: %v\n", err)
	}

	eventBus.Publish(event.NewEvent(event.NewGroupInvite, map[event.Field]string{event.TimestampReceived: time.Now().Format(time.RFC3339Nano), event.RemotePeer: ps1.GetProfileCopy(true).Onion, event.GroupInvite: string(invite)}))
	time.Sleep(1 * time.Second)

	eventBus.Publish(event.NewEvent(event.NewMessageFromGroup, map[event.Field]string{
		event.GroupID:           groupid,
		event.TimestampSent:     time.Now().Format(time.RFC3339Nano),
		event.TimestampReceived: time.Now().Format(time.RFC3339Nano),
		event.RemotePeer:        ps1.GetProfileCopy(true).Onion,
		event.Data:              testMessage,
	}))
	time.Sleep(1 * time.Second)

	ps1.Shutdown()

	ps2 := NewProfileWriterStore(eventBus, testingDir, password, nil)
	err = ps2.Load()
	if err != nil {
		t.Errorf("Error createing profileStore: %v\n", err)
	}

	profile = ps2.GetProfileCopy(true)
	if profile.Name != testProfileName {
		t.Errorf("Profile name from loaded profile incorrect. Expected: '%v' Actual: '%v'\n", testProfileName, profile.Name)
	}

	v, _ := profile.GetAttribute(testKey)
	if v != testVal {
		t.Errorf("Profile attribute '%v' inccorect. Expected: '%v' Actual: '%v'\n", testKey, testVal, v)
	}

	group2 := ps2.GetProfileCopy(true).Groups[groupid]
	if group2 == nil {
		t.Errorf("Group not loaded\n")
	}

}
