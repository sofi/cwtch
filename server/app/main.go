package main

import (
	cwtchserver "cwtch.im/cwtch/server"
	"git.openprivacy.ca/openprivacy/libricochet-go/connectivity"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"os"
	"path"
)

const (
	serverConfigFile = "serverConfig.json"
)

func main() {
	log.AddEverythingFromPattern("server/app/main")
	log.AddEverythingFromPattern("server/server")
	configDir := os.Getenv("CWTCH_CONFIG_DIR")

	serverConfig := cwtchserver.LoadConfig(configDir, serverConfigFile)

	acn, err := connectivity.StartTor(path.Join(configDir, "tor"), "")
	if err != nil {
		log.Errorf("\nError connecting to Tor: %v\n", err)
		os.Exit(1)
	}
	defer acn.Close()

	server := new(cwtchserver.Server)
	log.Infoln("starting cwtch server...")

	// TODO load params from .cwtch/server.conf or command line flag
	// TODO: respond to HUP so t.Close is gracefully called
	server.Run(acn, serverConfig)
}
