package server

import (
	"cwtch.im/cwtch/protocol"
	"cwtch.im/cwtch/server/metrics"
	"cwtch.im/cwtch/server/storage"
	"git.openprivacy.ca/openprivacy/libricochet-go/application"
	"os"
	"testing"
	"time"
)

func TestServerInstance(t *testing.T) {
	si := new(Instance)
	ai := new(application.Instance)
	ra := new(application.RicochetApplication)
	msi := new(storage.MessageStore)
	os.RemoveAll("messages")
	msi.Init(".", 5, metrics.NewCounter())
	gm := protocol.GroupMessage{
		Ciphertext: []byte("Hello this is a fairly average length message that we are writing here."),
		Spamguard:  []byte{},
	}

	si.Init(ai, ra, msi)
	msi.AddMessage(gm)
	res := si.HandleFetchRequest()

	if len(res) != 1 {
		t.Errorf("Expected 1 Group messages Instead got %v", res)
	}

	//	ra.HandleApplicationInstance(ai)
	si.HandleGroupMessage(&gm)

	time.Sleep(time.Second * 2)
}
